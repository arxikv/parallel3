#ifndef POISSON_H
#define POISSON_H

#include <mpi.h>
#include <cuda_runtime.h>

#define THREADS_PER_BLOCK 512

#include "custom_temporary_allocation.cu"

struct mpi_pair
{
    double beta;
    double gamma;
};

class Poisson
{
    // MPI
    int mpi_size;
    int mpi_rank;
    int mpi_size_x;  // >= mpi_size_y
    int mpi_size_y;
    int mpi_rank_x;
    int mpi_rank_y;
    int top;  // top neighbor rank, -1 if not exists
    int right;
    int bottom;
    int left;
    int reqs_count;
    MPI_Request *reqs_send;
    MPI_Request *reqs_recv;
    MPI_Datatype mpi_pair_type;
    MPI_Op merge_op;

    // Grid
    int grid_size;
    double grid_step;
    double sqr_grid_step;
    int brd_size_i;  // block size (y)
    int brd_size_j;  // block size (x)
    int brd_start_i;
    int brd_start_j;
    int ext_size_i;  // extended block size
    int ext_size_j;

    // Data (GPU)
    double *F;
    double *pk;
    double *pkp1;
    double *pdiff;  // pkp1 - pk
    double *r;
    double *g;
    double *dp;
    double *dr;
    double *dg;

    // Buffers (page-locked host memory)
    double *send_t;
    double *send_r;
    double *send_b;
    double *send_l;
    double *recv_t;
    double *recv_r;
    double *recv_b;
    double *recv_l;

    double dgg;
    double alpha_local;
    double alpha_global;
    mpi_pair beta_gamma;
    double tau;

    void init_mpi_size_xy();
    void init_neighbors();
    void init_brd_size_ij();

    cudaStream_t *streams;
    void init_cuda();

    void init_ext();  // uses kernels
    void init_mem();  // uses thrust
    void init_mpi_pair();
    void init_F();  // uses thrust

    void laplacian(double *dst, const double *src);  // uses kernel

    void update_r();  // uses thrust
    void update_g();  // uses thrust
    void update_p();  // uses thrust

    cached_allocator alloc;
    double dot(const double *a, const double *b);  // uses thrust

    double eps;
    double criterion();  // uses thrust
    void merge_alpha();
    void merge_beta_gamma();

    void to_buffers();  // uses kernels
    void from_buffers();  // uses kernels
    void exchange();

public:

    Poisson(int mpi_size_, int mpi_rank_, int grid_size_, double eps_);

    void init();

    void loop();

    void save(const char *path);

    void destroy();
};

#endif

