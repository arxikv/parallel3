ARCH=sm_20
HOST_COMP=mpicxx

all: poisson

poisson: poisson.cu main.cpp
	nvcc -arch=$(ARCH) -ccbin=$(HOST_COMP) -O3 poisson.cu main.cpp -o poisson

clean:
	rm -f poisson

