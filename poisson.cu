// Konstantin Arkhipenko, group 628

#include <cmath>
#include <fstream>
#include <iostream>

#include <sys/time.h>

#include "poisson.h"

#include <thrust/device_ptr.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/transform.h>
#include <thrust/fill.h>
#include <thrust/transform_reduce.h>

Poisson::Poisson(int mpi_size_, int mpi_rank_, int grid_size_, double eps_)
{
    mpi_size = mpi_size_;
    mpi_rank = mpi_rank_;
    grid_size = grid_size_;
    grid_step = 2.0 / grid_size;
    sqr_grid_step = grid_step * grid_step;
    eps = eps_;
}

void Poisson::init_mpi_size_xy()
{
    int tmp = mpi_size;
    bool odd = false;
    mpi_size_x = 1;
    mpi_size_y = mpi_size;

    while (tmp >>= 1) {
        if (odd = !odd) {
            mpi_size_x <<= 1;
            mpi_size_y >>= 1;
        }
    }

    // Now mpi_size_x is 2^{\lceil 0.5 * \log_2 mpi_size \rceil},
    // and mpi_size_y is mpi_size / mpi_size_x

    mpi_rank_x = mpi_rank % mpi_size_x;
    mpi_rank_y = mpi_rank / mpi_size_x;
}

void Poisson::init_neighbors()
{
    top = mpi_rank_y == 0 ? -1 : mpi_rank - mpi_size_x;
    right = mpi_rank_x == mpi_size_x - 1 ? -1 : mpi_rank + 1;
    bottom = mpi_rank_y == mpi_size_y - 1 ? -1 : mpi_rank + mpi_size_x;
    left = mpi_rank_x == 0 ? -1 : mpi_rank - 1;
}

void Poisson::init_brd_size_ij()
{
    int reg_size_i = (grid_size - 1) / mpi_size_y;
    int reg_size_j = (grid_size - 1) / mpi_size_x;
    int irreg_count_i = (grid_size - 1) % mpi_size_y;
    int irreg_count_j = (grid_size - 1) % mpi_size_x;

    if (mpi_rank_y < irreg_count_i) {
        brd_size_i = reg_size_i + 1;
        brd_start_i = mpi_rank_y * brd_size_i + 1;
    } else {
        brd_size_i = reg_size_i;
        brd_start_i = irreg_count_i * (reg_size_i + 1) + 1;
        brd_start_i += (mpi_rank_y - irreg_count_i) * brd_size_i;
    }

    if (mpi_rank_x < irreg_count_j) {
        brd_size_j = reg_size_j + 1;
        brd_start_j = mpi_rank_x * brd_size_j + 1;
    } else {
        brd_size_j = reg_size_j;
        brd_start_j = irreg_count_j * (reg_size_j + 1) + 1;
        brd_start_j += (mpi_rank_x - irreg_count_j) * brd_size_j;
    }

    ext_size_i = brd_size_i + 2;
    ext_size_j = brd_size_j + 2;
}

void Poisson::init_mem()
{
    int points_count = ext_size_i * ext_size_j;

    cudaMalloc(&F, points_count * sizeof(double));
    cudaMalloc(&pk, points_count * sizeof(double));
    thrust::device_ptr<double> pk_thr(pk);
    thrust::fill(pk_thr, pk_thr + points_count, 0);
    cudaMalloc(&pkp1, points_count * sizeof(double));
    cudaMalloc(&pdiff, points_count * sizeof(double));
    cudaMalloc(&r, points_count * sizeof(double));
    cudaMalloc(&g, points_count * sizeof(double));
    cudaMalloc(&dp, points_count * sizeof(double));
    cudaMalloc(&dr, points_count * sizeof(double));
    cudaMalloc(&dg, points_count * sizeof(double));

    if (top >= 0) {
        cudaHostAlloc(&send_t, brd_size_j * sizeof(double),
            cudaHostAllocMapped);
        cudaHostAlloc(&recv_t, brd_size_j * sizeof(double),
            cudaHostAllocMapped);
    }

    if (right >= 0) {
        cudaHostAlloc(&send_r, brd_size_i * sizeof(double),
            cudaHostAllocMapped);
        cudaHostAlloc(&recv_r, brd_size_i * sizeof(double),
            cudaHostAllocMapped);
    }

    if (bottom >= 0) {
        cudaHostAlloc(&send_b, brd_size_j * sizeof(double),
            cudaHostAllocMapped);
        cudaHostAlloc(&recv_b, brd_size_j * sizeof(double),
            cudaHostAllocMapped);
    }

    if (left >= 0) {
        cudaHostAlloc(&send_l, brd_size_i * sizeof(double),
            cudaHostAllocMapped);
        cudaHostAlloc(&recv_l, brd_size_i * sizeof(double),
            cudaHostAllocMapped);
    }

    reqs_send = new MPI_Request[4];
    reqs_recv = new MPI_Request[4];
}

void Poisson::destroy()
{
    cudaFree(F);
    cudaFree(pk);
    cudaFree(pkp1);
    cudaFree(pdiff);
    cudaFree(r);
    cudaFree(g);
    cudaFree(dp);
    cudaFree(dr);
    cudaFree(dg);

    if (top >= 0) {
        cudaFreeHost(send_t);
        cudaFreeHost(recv_t);
    }

    if (right >= 0) {
        cudaFreeHost(send_r);
        cudaFreeHost(recv_r);
    }

    if (bottom >= 0) {
        cudaFreeHost(send_b);
        cudaFreeHost(recv_b);
    }

    if (left >= 0) {
        cudaFreeHost(send_l);
        cudaFreeHost(recv_l);
    }

    delete []reqs_send;
    delete []reqs_recv;
    delete []streams;
}

void pair_add(void *in_, void *inout_, int *len, MPI_Datatype *dptr)
{
    mpi_pair *in = (mpi_pair *)in_;
    mpi_pair *inout = (mpi_pair *)inout_;
    for (int i = 0; i < *len; ++i) {
        inout[i].beta += in[i].beta;
        inout[i].gamma += in[i].gamma;
    }
}

void Poisson::init_mpi_pair()
{
    MPI_Type_contiguous(2, MPI_DOUBLE, &mpi_pair_type);
    MPI_Type_commit(&mpi_pair_type);
    MPI_Op_create(pair_add, true, &merge_op);
}

void Poisson::init_cuda()
{
    cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);
    cudaSetDeviceFlags(cudaDeviceMapHost);
    cudaSetDevice(mpi_rank % 2);

    streams = new cudaStream_t[4];
    for (int i = 0; i < 4; ++i)
        cudaStreamCreate(&streams[i]);
}

struct init_F_functor: public thrust::unary_function<int, double>
{
    const int ext_size_j;
    const int brd_start_i;
    const int brd_start_j;
    const double grid_step;

    init_F_functor(int ext_size_j_, int brd_start_i_,
        int brd_start_j_, double grid_step_):
        ext_size_j(ext_size_j_), brd_start_i(brd_start_i_),
        brd_start_j(brd_start_j_), grid_step(grid_step_) {}

    __host__ __device__
    double operator()(const int &ij) const { 
        int i = ij / ext_size_j;
        int j = ij % ext_size_j;
        double x = (brd_start_j + j - 1) * grid_step;
        double y = (brd_start_i + i - 1) * grid_step;
        double xpy2 = (x + y) * (x + y);
        return (4 - 8 * xpy2) * exp(1 - xpy2);
    }
};

void Poisson::init_F()
{
    int points_count = ext_size_i * ext_size_j;
    thrust::device_ptr<double> F_thr(F);
    thrust::counting_iterator<int> ij(0);
    thrust::transform(ij, ij + points_count, F_thr,
        init_F_functor(ext_size_j, brd_start_i, brd_start_j, grid_step));
}

__global__ void init_ext_hor(double *pk, double *pkp1,
    double *r, double *g, double *dp, double *dr, double *dg,
    const int offset, const int ext_size_j,
    const int brd_start_i, const int brd_start_j, const double grid_step)
{
    int ij = blockIdx.x * blockDim.x + threadIdx.x;

    if (ij < ext_size_j) {
        double x = (brd_start_j + ij - 1) * grid_step;
        double y = offset ? 2.0 : 0.0;
        double phi = exp(1 - (x + y) * (x + y));
        pk[ij + offset] = pkp1[ij + offset] = phi;
        r[ij + offset] = g[ij + offset] =
            dp[ij + offset] = dr[ij + offset] = dg[ij + offset] = 0.0;
    }
}

__global__ void init_ext_vert(double *pk, double *pkp1,
    double *r, double *g, double *dp, double *dr, double *dg,
    const int offset, const int ext_size_i, const int ext_size_j,
    const int brd_start_i, const int brd_start_j, const double grid_step)
{
    int ij = blockIdx.x * blockDim.x + threadIdx.x;

    if (ij < ext_size_i) {
        double x = offset ? 2.0 : 0.0;
        double y = (brd_start_i + ij - 1) * grid_step;
        double phi = exp(1 - (x + y) * (x + y));
        pk[ij * ext_size_j + offset] = pkp1[ij * ext_size_j + offset] = phi;
        r[ij * ext_size_j + offset] = g[ij * ext_size_j + offset] =
            dp[ij * ext_size_j + offset] = dr[ij * ext_size_j + offset] =
            dg[ij * ext_size_j + offset] = 0.0;
    }
}

void Poisson::init_ext()
{
    int block_count_hor = (ext_size_j - 1) / THREADS_PER_BLOCK + 1;
    int block_count_vert = (ext_size_i - 1) / THREADS_PER_BLOCK + 1;

    if (top < 0)
        init_ext_hor<<<block_count_hor, THREADS_PER_BLOCK, 0, streams[0]>>>(
            pk, pkp1, r, g, dp, dr, dg,
            0, ext_size_j, brd_start_i, brd_start_j, grid_step
        );
    if (left < 0)
        init_ext_vert<<<block_count_vert, THREADS_PER_BLOCK, 0, streams[1]>>>(
            pk, pkp1, r, g, dp, dr, dg,
            0, ext_size_i, ext_size_j, brd_start_i, brd_start_j, grid_step
        );
    if (bottom < 0)
        init_ext_hor<<<block_count_hor, THREADS_PER_BLOCK, 0, streams[2]>>>(
            pk, pkp1, r, g, dp, dr, dg,
            (ext_size_i - 1) * ext_size_j, ext_size_j,
            brd_start_i, brd_start_j, grid_step
        );
    if (right < 0)
        init_ext_vert<<<block_count_vert, THREADS_PER_BLOCK, 0, streams[3]>>>(
            pk, pkp1, r, g, dp, dr, dg,
            ext_size_j - 1, ext_size_i, ext_size_j,
            brd_start_i, brd_start_j, grid_step
        );

    for (int i = 0; i < 4; ++i)
        cudaStreamSynchronize(streams[i]);
}

void Poisson::init()
{
    init_mpi_size_xy();
    init_neighbors();
    init_brd_size_ij();
    init_cuda();
    init_mem();
    init_mpi_pair();
    init_F();
    init_ext();
}

__global__ void laplacian_kernel(double *dst, const double *src,
    const int ext_size_i, const int ext_size_j, const double sqr_grid_step)
{
    int ij = blockIdx.x * blockDim.x + threadIdx.x;
    int i = ij / ext_size_j;
    int j = ij % ext_size_j;

    if (i > 0 && j > 0 && i < ext_size_i - 1 && j < ext_size_j - 1) {
        dst[ij] = (
            (src[ij] - src[ij - ext_size_j]) -
            (src[ij + ext_size_j] - src[ij]) +
            (src[ij] - src[ij - 1]) -
            (src[ij + 1] - src[ij])
        ) / sqr_grid_step;
    }
}

void Poisson::laplacian(double *dst, const double *src)
{
    int points_count = ext_size_i * ext_size_j;
    int blocks_count = (points_count - 1) / THREADS_PER_BLOCK + 1;

    laplacian_kernel<<<blocks_count, THREADS_PER_BLOCK>>>(dst, src,
        ext_size_i, ext_size_j, sqr_grid_step);
}

struct saxpy_functor
{
    const double coeff;
    const int first_i;
    const int last_i;
    const int first_j;
    const int last_j;
    const int ext_size_j;

    saxpy_functor(double coeff_, int first_i_, int last_i_,
        int first_j_, int last_j_, int ext_size_j_):
        coeff(coeff_), first_i(first_i_), last_i(last_i_),
        first_j(first_j_), last_j(last_j_), ext_size_j(ext_size_j_) {}

    template<typename Tuple>
    __host__ __device__
    double operator()(const Tuple &t) const { 
        int ij = thrust::get<2>(t);
        int i = ij / ext_size_j;
        int j = ij % ext_size_j;

        if (i >= first_i && i <= last_i && j >= first_j && j <= last_j)
            return thrust::get<0>(t) + coeff * thrust::get<1>(t);
        else
            return thrust::get<3>(t);
    }
};

void Poisson::update_r()
{
    thrust::device_ptr<double> dp_thr(dp);
    thrust::device_ptr<double> F_thr(F);
    thrust::device_ptr<double> r_thr(r);
    thrust::counting_iterator<int> ij(0);

    int first_i = top < 0 ? 1 : 0;
    int last_i = bottom < 0 ? brd_size_i : ext_size_i - 1;
    int first_j = left < 0 ? 1 : 0;
    int last_j = right < 0 ? brd_size_j : ext_size_j - 1;
    int points_count = ext_size_i * ext_size_j;

    thrust::transform(
        thrust::make_zip_iterator(
            thrust::make_tuple(dp_thr, F_thr, ij, r_thr)),
        thrust::make_zip_iterator(
            thrust::make_tuple(
                dp_thr + points_count, F_thr + points_count,
                ij + points_count, r_thr + points_count)),
        r_thr,
        saxpy_functor(-1.0, first_i, last_i, first_j, last_j, ext_size_j)
    );
}

void Poisson::update_g()
{
    thrust::device_ptr<double> r_thr(r);
    thrust::device_ptr<double> g_thr(g);
    thrust::counting_iterator<int> ij(0);

    int first_i = top < 0 ? 1 : 0;
    int last_i = bottom < 0 ? brd_size_i : ext_size_i - 1;
    int first_j = left < 0 ? 1 : 0;
    int last_j = right < 0 ? brd_size_j : ext_size_j - 1;
    int points_count = ext_size_i * ext_size_j;

    thrust::transform(
        thrust::make_zip_iterator(
            thrust::make_tuple(r_thr, g_thr, ij, g_thr)),
        thrust::make_zip_iterator(
            thrust::make_tuple(
                r_thr + points_count, g_thr + points_count,
                ij + points_count, g_thr + points_count)),
        g_thr,
        saxpy_functor(-alpha_global,
            first_i, last_i, first_j, last_j, ext_size_j)
    );
}

void Poisson::update_p()
{
    thrust::device_ptr<double> pk_thr(pk);
    thrust::device_ptr<double> pkp1_thr(pkp1);
    thrust::device_ptr<double> g_thr(g);
    thrust::counting_iterator<int> ij(0);

    int first_i = top < 0 ? 1 : 0;
    int last_i = bottom < 0 ? brd_size_i : ext_size_i - 1;
    int first_j = left < 0 ? 1 : 0;
    int last_j = right < 0 ? brd_size_j : ext_size_j - 1;
    int points_count = ext_size_i * ext_size_j;

    thrust::transform(
        thrust::make_zip_iterator(
            thrust::make_tuple(pk_thr, g_thr, ij, pkp1_thr)),
        thrust::make_zip_iterator(
            thrust::make_tuple(
                pk_thr + points_count, g_thr + points_count,
                ij + points_count, pkp1_thr + points_count)),
        pkp1_thr,
        saxpy_functor(-tau, first_i, last_i, first_j, last_j, ext_size_j)
    );
}

struct dot_functor
{
    const int ext_size_i;
    const int ext_size_j;

    dot_functor(int ext_size_i_, int ext_size_j_):
        ext_size_i(ext_size_i_), ext_size_j(ext_size_j_) {}

    template<typename Tuple>
    __host__ __device__
    double operator()(const Tuple &t) const { 
        int ij = thrust::get<2>(t);
        int i = ij / ext_size_j;
        int j = ij % ext_size_j;

        if (i > 0 && i < ext_size_i - 1 && j > 0 && j < ext_size_j - 1)
            return thrust::get<0>(t) * thrust::get<1>(t);
        else
            return 0.0;
    }
};

double Poisson::dot(const double *a, const double *b)
{
    thrust::device_ptr<const double> a_thr(a);
    thrust::device_ptr<const double> b_thr(b);
    thrust::counting_iterator<int> ij(0);
    int points_count = ext_size_i * ext_size_j;

    double result = thrust::transform_reduce(thrust::cuda::par(alloc),
        thrust::make_zip_iterator(
            thrust::make_tuple(a_thr, b_thr, ij)),
        thrust::make_zip_iterator(
            thrust::make_tuple(a_thr + points_count, b_thr + points_count,
                ij + points_count)),
        dot_functor(ext_size_i, ext_size_j), 0.0, thrust::plus<double>()
    );

    return sqr_grid_step * result;
}

double Poisson::criterion()
{
    double local, global = 0.0;
    thrust::device_ptr<double> pk_thr(pk);
    thrust::device_ptr<double> pkp1_thr(pkp1);
    thrust::device_ptr<double> pdiff_thr(pdiff);
    thrust::counting_iterator<int> ij(0);
    int points_count = ext_size_i * ext_size_j;

    thrust::transform(pkp1_thr, pkp1_thr + points_count,
        pk_thr, pdiff_thr, thrust::minus<double>());

    local = dot(pdiff, pdiff);
    MPI_Allreduce(&local, &global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    return sqrt(global);
}

void Poisson::merge_alpha()
{
    alpha_global = 0.0;
    MPI_Allreduce(&alpha_local, &alpha_global, 1, MPI_DOUBLE, MPI_SUM,
        MPI_COMM_WORLD);
    alpha_global /= dgg;
}

void Poisson::merge_beta_gamma()
{
    mpi_pair result;
    result.beta = result.gamma = 0.0;

    MPI_Allreduce(&beta_gamma, &result, 1, mpi_pair_type, merge_op,
        MPI_COMM_WORLD);

    dgg = result.gamma;
    tau = result.beta / result.gamma;
}

__global__ void buff_hor(double *dst, const double *src,
    const int brd_size_j)
{
    int ij = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (ij < brd_size_j) {
        dst[ij] = src[ij];
    }
}

__global__ void to_buff_vert(double *dst, const double *src,
    const int brd_size_i, const int ext_size_j)
{
    int ij = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (ij < brd_size_i) {
        dst[ij] = src[ij * ext_size_j];
    }
}

__global__ void from_buff_vert(double *dst, const double *src,
    const int brd_size_i, const int ext_size_j)
{
    int ij = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (ij < brd_size_i) {
        dst[ij * ext_size_j] = src[ij];
    }
}

// Copies border of the block to send buffers
void Poisson::to_buffers()
{
    int block_count_hor = (brd_size_j - 1) / THREADS_PER_BLOCK + 1;
    int block_count_vert = (brd_size_i - 1) / THREADS_PER_BLOCK + 1;

    if (top >= 0)
        buff_hor<<<block_count_hor, THREADS_PER_BLOCK, 0, streams[0]>>>(
            send_t, &dp[ext_size_j + 1], brd_size_j
        );
    if (left >= 0)
        to_buff_vert<<<block_count_vert, THREADS_PER_BLOCK, 0, streams[1]>>>(
            send_l, &dp[ext_size_j + 1], brd_size_i, ext_size_j
        );
    if (bottom >= 0)
        buff_hor<<<block_count_hor, THREADS_PER_BLOCK, 0, streams[2]>>>(
            send_b, &dp[brd_size_i * ext_size_j + 1], brd_size_j
        );
    if (right >= 0)
        to_buff_vert<<<block_count_vert, THREADS_PER_BLOCK, 0, streams[3]>>>(
            send_r, &dp[2 * ext_size_j - 2], brd_size_i, ext_size_j
        );

    for (int i = 0; i < 4; ++i)
        cudaStreamSynchronize(streams[i]);
}

// Copies recv buffers to border of *extended* block
void Poisson::from_buffers()
{
    int block_count_hor = (brd_size_j - 1) / THREADS_PER_BLOCK + 1;
    int block_count_vert = (brd_size_i - 1) / THREADS_PER_BLOCK + 1;

    if (top >= 0)
        buff_hor<<<block_count_hor, THREADS_PER_BLOCK, 0, streams[0]>>>(
            &dp[1], recv_t, brd_size_j
        );
    if (left >= 0)
        from_buff_vert<<<block_count_vert, THREADS_PER_BLOCK, 0, streams[1]>>>(
            &dp[ext_size_j], recv_l, brd_size_i, ext_size_j
        );
    if (bottom >= 0)
        buff_hor<<<block_count_hor, THREADS_PER_BLOCK, 0, streams[2]>>>(
            &dp[(ext_size_i - 1) * ext_size_j + 1], recv_b, brd_size_j
        );
    if (right >= 0)
        from_buff_vert<<<block_count_vert, THREADS_PER_BLOCK, 0, streams[3]>>>(
            &dp[2 * ext_size_j - 1], recv_r, brd_size_i, ext_size_j
        );

    for (int i = 0; i < 4; ++i)
        cudaStreamSynchronize(streams[i]);
}

void Poisson::exchange()
{
    reqs_count = 0;

    if (top >= 0) {
        MPI_Isend(send_t, brd_size_j, MPI_DOUBLE, top, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_t, brd_size_j, MPI_DOUBLE, top, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }

    if (right >= 0) {
        MPI_Isend(send_r, brd_size_i, MPI_DOUBLE, right, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_r, brd_size_i, MPI_DOUBLE, right, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }

    if (bottom >= 0) {
        MPI_Isend(send_b, brd_size_j, MPI_DOUBLE, bottom, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_b, brd_size_j, MPI_DOUBLE, bottom, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }

    if (left >= 0) {
        MPI_Isend(send_l, brd_size_i, MPI_DOUBLE, left, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_l, brd_size_i, MPI_DOUBLE, left, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }

    MPI_Waitall(reqs_count, reqs_send, MPI_STATUS_IGNORE);
    MPI_Waitall(reqs_count, reqs_recv, MPI_STATUS_IGNORE);
}

void swap(double **a, double **b)
{
    double *tmp = *a;
    *a = *b;
    *b = tmp;
}

void Poisson::loop()
{
    int iteration = 0;
    struct timeval t0, t1;
    gettimeofday(&t0, 0);

    while (true)
    {
        ++iteration;

        laplacian(dp, pk);

        to_buffers();
        exchange();
        from_buffers();

        update_r();

        if (iteration > 1) {
            laplacian(dr, r);
            alpha_local = dot(dr, g);
            merge_alpha();
            update_g();
            beta_gamma.beta = dot(r, g);
        } else {
            swap(&r, &g);
            beta_gamma.beta = dot(g, g);
        }

        laplacian(dg, g);
        beta_gamma.gamma = dot(dg, g);
        merge_beta_gamma();
        update_p();

        if (criterion() < eps)
            break;

        swap(&pk, &pkp1);
    }

    gettimeofday(&t1, 0);
    double total_time = t1.tv_sec - t0.tv_sec +
        1e-6 * (t1.tv_usec - t0.tv_usec);

    if (mpi_rank == 0) {
        std::cout << iteration << " iterations completed in " <<
            total_time << " s" << std::endl;
    }
}

void Poisson::save(const char *path)
{
    int first_i = top < 0 ? 0 : 1;
    int last_i = bottom < 0 ? ext_size_i - 1 : brd_size_i;
    int first_j = left < 0 ? 0 : 1;
    int last_j = right < 0 ? ext_size_j - 1 : brd_size_j;
    int points_count = ext_size_i * ext_size_j;
    MPI_Status status;

    double *phost;
    cudaHostAlloc(&phost, points_count * sizeof(double), cudaHostAllocMapped);
    cudaMemcpy(phost, pkp1, points_count * sizeof(double),
        cudaMemcpyDeviceToHost);

    if (mpi_rank == 0) {
        std::ofstream f(path, std::ios::out | std::ios::binary);

        for (int i = first_i; i <= last_i; ++i) {
            for (int j = first_j; j <= last_j; ++j) {
                int global_i = brd_start_i + i - 1;
                int global_j = brd_start_j + j - 1;

                f.write((char *)&global_i, sizeof(int));
                f.write((char *)&global_j, sizeof(int));
                f.write((char *)&phost[i * ext_size_j + j], sizeof(double));
            }
        }

        for (int r = 1; r < mpi_size; ++r) {
            MPI_Recv(&ext_size_i, 1, MPI_INT, r, 1, MPI_COMM_WORLD, &status);
            MPI_Recv(&ext_size_j, 1, MPI_INT, r, 2, MPI_COMM_WORLD, &status);
            MPI_Recv(&brd_start_i, 1, MPI_INT, r, 3, MPI_COMM_WORLD, &status);
            MPI_Recv(&brd_start_j, 1, MPI_INT, r, 4, MPI_COMM_WORLD, &status);

            MPI_Recv(phost, ext_size_i * ext_size_j, MPI_DOUBLE, r, 0,
                MPI_COMM_WORLD, &status);

            first_i = (r / mpi_size_x == 0) ? 0 : 1;
            last_i = (r / mpi_size_x == mpi_size_y - 1) ? ext_size_i - 1 :
                ext_size_i - 2;
            first_j = (r % mpi_size_x == 0) ? 0 : 1;
            last_j = (r % mpi_size_x == mpi_size_x - 1) ? ext_size_j - 1 :
                ext_size_j - 2;

            for (int i = first_i; i <= last_i; ++i) {
                for (int j = first_j; j <= last_j; ++j) {
                    int global_i = brd_start_i + i - 1;
                    int global_j = brd_start_j + j - 1;

                    f.write((char *)&global_i, sizeof(int));
                    f.write((char *)&global_j, sizeof(int));
                    f.write((char *)&phost[i * ext_size_j + j], sizeof(double));
                }
            }
        }

        f.close();
    } else {
        MPI_Send(&ext_size_i, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
        MPI_Send(&ext_size_j, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);
        MPI_Send(&brd_start_i, 1, MPI_INT, 0, 3, MPI_COMM_WORLD);
        MPI_Send(&brd_start_j, 1, MPI_INT, 0, 4, MPI_COMM_WORLD);

        MPI_Send(phost, points_count, MPI_DOUBLE, 0, 0,
            MPI_COMM_WORLD);
    }

    cudaFreeHost(phost);
}

