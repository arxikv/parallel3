import struct
import sys
import numpy as np


grid_size = int(sys.argv[1])
bin_file = sys.argv[2]
text_file = sys.argv[3]

p = []
u = []

with open(bin_file, 'rb') as fi:
    for _ in range((grid_size + 1) * (grid_size + 1)):
        i = struct.unpack('i', fi.read(4))[0]
        j = struct.unpack('i', fi.read(4))[0]
        x = 2.0 * j / grid_size
        y = 2.0 * i / grid_size
        p_val = struct.unpack('d', fi.read(8))[0]
        p.append((x, y, p_val))
        u_val = np.exp(1.0 - (x + y) ** 2)
        u.append((x, y, u_val))

    p = sorted(p, key=lambda a: (a[0], a[1]))
    u = sorted(u, key=lambda a: (a[0], a[1]))

    error = np.array([it[2] for it in p]) - np.array([it[2] for it in u])
    error = np.linalg.norm(error * 2.0 / grid_size)
    print('Error: %.6f' % error)

    with open(text_file + '_p', 'w') as fo:
        prev_x = 0.0
        for x, y, z in p:
            if x != prev_x:
                prev_x = x
                fo.write('\n')
            fo.write('{0} {1} {2}\n'.format(x, y, z))

    with open(text_file + '_u', 'w') as fo:
        prev_x = 0.0
        for x, y, z in u:
            if x != prev_x:
                prev_x = x
                fo.write('\n')
            fo.write('{0} {1} {2}\n'.format(x, y, z))

