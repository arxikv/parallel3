#include <cstdlib>
#include <iostream>

#include "poisson.h"

int main(int argc, char **argv)
{
    int mpi_size;
    int mpi_rank;
    double eps = atof(argv[1]);

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    if (argc > 2) {
        int grid_size = atoi(argv[2]);
        const char *path = argv[3];

        Poisson poisson(mpi_size, mpi_rank, grid_size, eps);
        poisson.init();
        poisson.loop();
        poisson.save(path);
        poisson.destroy();
    } else {
        for (int grid_size = 1000; grid_size <= 5000; grid_size += 1000) {
            if (mpi_rank == 0)
                std::cout << "Grid size " << grid_size << std::endl;
            Poisson poisson(mpi_size, mpi_rank, grid_size, eps);
            poisson.init();
            poisson.loop();
            poisson.destroy();
        }
    }

    MPI_Finalize();

    return 0;
}

